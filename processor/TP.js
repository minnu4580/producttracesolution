/* Transaction Processor */
const {TextDecoder} = require('text-encoding/lib/encoding')
const { TransactionHandler } = require('sawtooth-sdk/processor/handler')
const {hash , writeToStore ,getProductAddress } = require('./lib/transaction')
const { TransactionProcessor } = require('sawtooth-sdk/processor');



const FAMILY_NAME = "Product Chain"
const NAMESPACE = hash(FAMILY_NAME).substring(0, 6);
const URL = 'tcp://validator:4004';
var decoder = new TextDecoder('utf8')

/* function to add tested product data to chain
parameter :
context - validator context object
lab - name of Lab
proNumber - product id number
dom - date of processing
model - product type
proCode - product lab code 
*/      

function addTest (context,lab,proNumber,dom,model,proCode) {
    let product_Address = getProductAddress(proNumber)
    let product_detail =[lab,proNumber,dom,model,proCode]
    return writeToStore(context,product_Address,product_detail)
}

/* function to add  audited product data to chain
parameter :
context - validator context object
Auditer - Auditing authority      
proNumber - product id Number
dor - date of auditing
owner - auditer name 
auditNumber - auditer id number 
*/

function auditProduct(context,proNumber,dor,owner,auditNumber,Auditer,OwnerAddress){
    console.log("auditing product")
    let address = getProductAddress(proNumber)
    return context.getState([address]).then(function(data){
    console.log("data",data)
    if(data[address] == null || data[address] == "" || data[address] == []){
        console.log("Invalid product number!")
    }else{
    let stateJSON = decoder.decode(data[address])
    let newData = stateJSON + "," + [dor,owner,auditNumber,Auditer,OwnerAddress].join(',')
    return writeToStore(context,address,newData)
    }
    })         
}


//transaction handler class

class Product extends TransactionHandler{
    constructor(){
        super(FAMILY_NAME, ['1.0'], [NAMESPACE]);
    

    }
//apply function
    apply(transactionProcessRequest, context){
        let PayloadBytes = decoder.decode(transactionProcessRequest.payload)
        let Payload = PayloadBytes.toString().split(',')
        let action = Payload[0]
        if (action === "Test Product"){
            return addProduct(context,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5])
        }
        else if(action === "Audit Product"){
            return auditProduct(context,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5],Payload[6])
        }
        
    }
}

const transactionProcesssor = new TransactionProcessor(URL);
transactionProcesssor.addHandler(new Vehicle);
transactionProcesssor.start();


