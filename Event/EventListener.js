/* Creates custom event subscription to application specific events */
/* event alerts when a word of length between 5 and 9 is submitted */

const {
    Message,
    EventFilter,
    EventList,
    EventSubscription,
    ClientEventsSubscribeRequest,
    ClientEventsSubscribeResponse
  } = require('sawtooth-sdk/protobuf');

const { TextDecoder } = require('text-encoding/lib/encoding')
var decoder = new TextDecoder('utf8')
const VALIDATOR_URL = "tcp://validator:4004"
const { Stream } = require('sawtooth-sdk/messaging/stream'); 

// returns the subscription request status 
function checkStatus(response){
        let msg = ""
        if (response.status === 0){
                msg = 'subscription : OK'
        } else if (response.status === 1){
                msg = 'subscription : GOOD '
        } else {
                msg = 'subscription failed !'
        }
        return msg
}
        

//event message handler 
function getEventsMessage(message){
        // Write your event handling code here
        let eventlist = EventList.decode(message.content).events
        eventlist.map(function(event){
                if(event.eventType == 'sawtooth/block-commit'){
                        console.log('Event',event);
                }
                else if(event.eventlist == 'HelloWorld/WordLength'){
                        console.log('Word length event',event);
                }
        }) 
}


function EventSubscribe(URL){
        // Write your code here
        let Stream = new Stream(URL);
        const blockCommitSubsciption = EventSubscription.create({
                eventType:'sawtooth/block-commit'
        });
        const wordLenthSubscription =EventSubscription.create({
                eventType:'ProductTrace/WordLength',
                filters : [EventFilter.create({
                        key: 'message-length',
                        matchString:'^[5-9]\d*$' ,
                        filterType : EventFilter.FilterType.REGEX_ANY

                })]
        })
        const subscription_request = ClientEventsSubscribeRequest.encode({
                subsciptions : [blockCommitSubsciption,wordLenthSubscription]
        }).finish();
        stream.connect(() =>{
                stream.send(Message.MessageType.CLIENTS_EVENTS_SUBSCRIBE_REQUEST,subscription_request)
                stream.onReceive(getEventsMessage)
                .then (function(response){
                        return ClientEventsSubscribeResponse.decode(response)
                })
                .then (function(decoded_Response){
                        console.log(checkStatus(decoded_Response))
                })
                             
        })    
        console.log("Inside here");
}


EventSubscribe(VALIDATOR_URL);
