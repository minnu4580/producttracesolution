function viewData() {
    window.location.href='/listView';

}

function getProductAddress(proNumber,LabName){    
    let keyHash  = hash(getUserPublicKey(LabName))
    let nameHash = hash("Testing Chain")
    let proHash = hash(proNumber)
    return nameHash.slice(0,6) +proHash.slice(0,4)+keyHash.slice(0,60)

}

function addProductAsTested(event){
    event.preventDefault();
    let privKey = document.getElementById('manPrivKey').value;
    let ProNumb = document.getElementById('ProNumbMan').value;
    let ProCode = document.getElementById('ProCode').value;
    let makeModel = document.getElementById('m&m').value;
    let dom = document.getElementById('dom').value;
    $.post('/addTest',{ key: privKey,pro:ProNumb,code:ProCode,model:makeModel,date:dom } ,'json');
    
}
function addProductAsAudit(event) {

    event.preventDefault();
    let privKey = document.getElementById('regPrivKey').value;
    let ProNumb = document.getElementById('ProNumbMan').value;
    let OwnName = document.getElementById('OwnName').value;
    let address = document.getElementById('address').value;
    let DOR = document.getElementById('dor').value;
    let manName = document.getElementById('manName').value;
    let AuditNo = document.getElementById('numAudit').value;
    $.post('/auditProduct',{ key: privKey,pro:ProNumb,OwnerName:OwnName,addr:address,Dor:DOR,name:manName,audit:PlateNo } ,'json');
    
}
