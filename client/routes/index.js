var express = require('express');
var{ Vehicle }= require('./UserClient')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('dashboards', { title: 'Dashboards' });
});

////
router.get('/listProducts', async (req,res)=>{
  var productClient = new Product();
  let stateData = await productClient.getProductListings();
  //console.log("listings", stateData);
  let productsList = [];
  stateData.data.forEach(products => {
    if(!products.data) return;
    let decodedProducts = Buffer.from(product.data, 'base64').toString();
    let productDetails = decodedProducts.split(',');

    //console.log("decodedProducts------", decodedProducts);
    vehiclesList.push({
      ProNum: productDetails[1],
      codeNo: productDetails[4],
      model: productDetails[3],
      dom: productDetails[2],
      status: (productDetails.length === 5)?"Not Audited":"Audited",
      engineer: productDetails[6],
      address: productDetails[9],
      dor: productDetails[5],
      numberAud: productDetails[7]
    });
  });

  res.render('productList', { listings: productsList });
});


router.get('/homePage',(req,res)=>{
  res.render('dashboards', { title: 'Dashboards' });
});

//////


router.post('/testProduct',function(req, res){
  let key = req.body.key
  let pro = req.body.vin
  let model = req.body.model
  let dom = req.body.date
  let codeNo = req.body.engine
  console.log("Data sent to REST API");
  var client = new Product();
  client.addProduct("Testing",key,pro,dom,model,codeNo)
  res.send({message: "Data successfully added"});
})
router.post('/auditProduct',function(req, res){
  let key = req.body.key
  let pro = req.body.pro
  let OwnName = req.body.EngineerName
  let dor = req.body.Dor
  let auditNo = req.body.audit
  let address = req.body.addr
  console.log("Data sent to REST API");
  var client = new Product();
  client.auditProduct("Audit team",key,pro,dor,OwnName,auditNo,address)
  res.send({message: "Data successfully added"});
})
module.exports = router;
