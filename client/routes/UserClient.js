const {createTransaction} = require('./lib/processor')
const {hash,getProductAddress} = require('./lib/transaction')
const fetch = require('node-fetch');

//Approved keys for manufacturer and auditer
TESTERKEY = '8f99bb8b1dc799fd1ed9b7e370330f9378c78f7c332ac3e2233bf559ce21ea8b'
AUDITERKEY = '4206848f09f0953370fc3e4a131faeab07e239d451190294e5049cfcf05a107e'

//family name
FAMILY_NAME='Product Chain'

// class for product
class Product{

      /* function to add new test produt data to chain
      parameters :
      manufacturer - name of the lab
      Key - key that is available to the lab
      proNumber - product idNumber
      dom - date of testing
      model - product type 
      productCode - lab codeof products
      */
      addTest(lab,Key,proNumber,dom,model,productCode){

        let address = getProductAddress(proNumber)
        let action = "Test Product"
        let payload = [action,lab,proNumber,dom,model,productCode].join(',')
        if (Key == TESTERKEY){
        	createTransaction(FAMILY_NAME,[address],[address],Key,payload)}
        else{
        	console.log('tester Not Authorised')
        }

      }
      /* function to add audit details of tested product to chain
      parameters :
      Auditer - Auditing authority
      auditerKey - key of auditer
      proNumber - product proNumber
      dor - date of auditing
      owner - auditer name 
      auditNumber - auditer id number 
      address - auditer designation address
      */

      auditProduct(Auditer,auditerKey,proNumber,dor,owner,auditNumber,address){
        let action = "Audit Product"
        let Address = getProductAddress(proNumber)
        let payload = [action,proNumber,dor,owner,auditNumber,Auditer,address].join(',')
        if (auditerKey == AUDITERKEY){
        	createTransaction(FAMILY_NAME,[Address],[Address],auditerKey,payload)
        }
        else{
        	console.log('Auditer Not Authorised')
        }
        

    }


//////

/**
 * Get state from the REST API
 * @param {*} address The state address to get
 * @param {*} isQuery Is this an address space query or full address
 */
async getState (address, isQuery) {
  let stateRequest = 'http://rest-api:8008/state';
  if(address) {
    if(isQuery) {
      stateRequest += ('?address=')
    } else {
      stateRequest += ('/address/');
    }
    stateRequest += address;
  }
  let stateResponse = await fetch(stateRequest);
  let stateJSON = await stateResponse.json();
  return stateJSON;
}

async getProductListings() {
  let productListingAddress = hash(FAMILY_NAME).substr(0, 6);
  return this.getState(productListingAddress, true);
}

//////

}

module.exports = {Product};
